function demo(label_test)
% Demo function for running MFSF (MultiFrame SubspaceFlow) and visualising
% the result. For more details (how to set parameters etc.), type "help
% runMFSF" and "help visualizeMFSF" or see comments
% within runMFSF.m and visualizeMFSF. 
%
% label_test: label for the test experiment you want to run:
%
%   - if 1: run MFSF in a face sequence to test if the code is
%   running in your system and whether you have enough memory (this is just
%   for fast testing - expect dummy results).
%
%   - if 2: run MFSF in the same sequence with parameteres that make sense
%   (expect good results).
%
%   - if 3: run MFSF in a different face sequence that contains precomputed sparse
%   landmarks and use them as priors for the optical flow.
%
%   - if 4: run MFSF in a back sequence by using flag_grad=0 (RGB channels
%   for the image data term) instead of the default flag_grad=2 (gradient
%   of the image channels), which leads to a lighter version of the
%   algorithm, in terms of memory requirements and runtime.
%
%
% Note that this demo requires a GPU card with more than 1 GB memory. In
% case you have memory problems, consider decreasing the input parameter
% MaxPIXpyr.

switch label_test
    
    case 1 % Test 1
        path_in = 'in_test';
        
        % Test if code runs OK by calling MFSF with MaxPL=1 (which means that we go directly to finest pyramid
        % level, which needs the maximum of memory) and W=1 (only 1 warping iteration, for
        % fast test), and ignoring the result:
        runMFSF('path_in',path_in,'frname_frmt','face%04d.jpg', 'nref', 89, 'sframe', 76, 'nframe', 120,...
            'MaxPIXpyr', 20000, 'MaxPL',1, 'W',1);
        
        fprintf('Code runs OK!\n');
                    
        
    case 2 % Test 2    
        path_in = 'in_test';
        path_res = 'out_test2'; 
        
        % Run MFSF (same command as in case 1, but we have deleted the
        % specification of MaxPL,W), with default parameters that will create good
        % results (limiting the coarse-to-fine strategy to process pyramid levels with at most
        % MaxPIXpyr=20000 pixels):
        [u,v,parmsOF,info] = runMFSF('path_in',path_in,'frname_frmt','face%04d.jpg', 'nref', 89, 'sframe', 76, 'nframe', 120,...
            'MaxPIXpyr', 20000);
        
        % Save the result:
        mkdir(path_res);
        save(fullfile(path_res,'result.mat'), 'u', 'v', 'parmsOF','info');
        
        % Visualise the result (note that the current visualisation code is
        % slow, mainly due to grid visualisation, so for faster runtimes
        % set type_vis='colcode,warp' or avoid visualisations completely):
        path_figs = fullfile(path_res,'figures');
        % use an image with a mask for reference frame in order to
        % visualise the grid only for the foreground:
        tic; visualizeMFSF(path_in,u,v,parmsOF, 'path_figs',path_figs, 'Nrows_grid', 60,...
            'file_mask_grid','in_test/mask_f89.png'); 
        fprintf('\nRuntime of MFSF algorithm: %g sec (%g sec per frame)\nRuntime of visualisation code: %g sec\n',info.runtime,info.runtime/parmsOF.nframe,toc);
                
        
    case 3 % Test 3
        path_in = 'in_test_landm';
        path_res = 'out_test3'; 
        
        % Run MFSF in the sequence within the directory "in_test_landm/",
        % which also contains files with landmark points per frame.
        % Incorporate the landmarks to act as priors for the optical flow
        % in the corresponding sparse points.         
        [u,v,parmsOF,info] = runMFSF('path_in',path_in,'frname_frmt','%03d.jpg', 'nref', 84, 'sframe', 5, 'nframe', 80,...
            'MaxPIXpyr', 12000, ...
            'lndm_fname_frmt', '%03d.pts');        
        
        % Save the result:
        mkdir(path_res);
        save(fullfile(path_res,'result.mat'), 'u', 'v', 'parmsOF','info');
        
        % Visualise the result:
        path_figs = fullfile(path_res,'figures');
        % call visualizeMFSF as before, but additionally specify the
        % variables inds_lndm2D,landmarks_seq that determine the landmarks'
        % position in every frame and their relation to the image pixels:
        tic; visualizeMFSF(path_in,u,v,parmsOF, 'path_figs',path_figs, ...
            'inds_lndm2D',info.inds_lndm2D, 'landmarks_seq',info.landmarks_seq);                                                          
        fprintf('\nRuntime of MFSF algorithm: %g sec (%g sec per frame)\nRuntime of visualisation code: %g sec\n',info.runtime,info.runtime/parmsOF.nframe,toc);
        
                
    case 4 % Test 4
        path_in = 'in_test_back';
        path_res = 'out_test4'; 
        
        % Run MFSF, but use flag_grad=0 which requires less memory and runs
        % faster (this choice makes the image data term less robust to
        % illumination changes, but in this sequence this is not a problem
        % due to very good texture information):
        [u,v,parmsOF,info] = runMFSF('path_in',path_in,'frname_frmt','img_%04d.jpg', 'nref', 9, 'sframe', 1, 'nframe', 150,...
            'MaxPIXpyr', 20000, 'flag_grad', 0);
        
        % Save the result:
        mkdir(path_res);
        save(fullfile(path_res,'result.mat'), 'u', 'v', 'parmsOF','info');
        
        % Visualise the result:
        path_figs = fullfile(path_res,'figures');
        tic; visualizeMFSF(path_in,u,v,parmsOF, 'path_figs',path_figs, ...
            'gcolor', 'g', ... % use a grid colour that is more visible for this sequence
            'file_mask_grid','in_test_back/mask_f9.png');
        fprintf('\nRuntime of MFSF algorithm: %g sec (%g sec per frame)\nRuntime of visualisation code: %g sec\n',info.runtime,info.runtime/parmsOF.nframe,toc);
                 
                
end