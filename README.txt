This set of functions implements the algorithm of Multi-Frame
Subspace Flow (MFSF), for estimation of multi-frame optical flow of
an image sequence (or equivalently the estimation of dense 2D
tracks and dense registration of each frame of the sequence to a
reference frame).


REFERENCE
---------

This method has been presented in the following journal article:

R. Garg, A. Roussos, L. Agapito, "A Variational Approach to Video
Registration with Subspace Constraints", International Journal of
Computer Vision (IJCV), vol. 104, issue 3, pp. 286-314, Sept. 2013.

If you use this code for a scientific publication, please reference
the above paper.


HOW TO RUN & DOCUMENTATION
--------------------------

This code is in MATLAB, using CUDA kernels and requiring an NVIDIA
GPU card.

For more details and further demonstration of the usage of this
code, please see the function demo.m, where you can find several
different test cases to run.

The main function that runs this code is runMFSF.m. Detailed
documentation on its usage is provided within this function. You
can also type "help runMFSF" from MATLAB's command window.


FORMAT OF INPUT FILES
---------------------

For the input image sequence, there should be an image file per
frame of the sequence. All image file formats that can be read by
MATLAB's function 'imread' are supported, e.g. JPEG, PNG, etc. Both
colour and grayscale images are supported (in the latter case, a
faster version of the algorithm is used). The values in these
images should be in the range [0,255]. A consistent naming format
of the files should be used, e.g. 'img_%04d.jpg', where '%04d'
stands for the frame index, which should be ascending with constant
steps (e.g. 10, 15, 20, ..., which in this example would correspond
to image file names 'img_0010.jpg', 'img_0015.jpg', 'img_0020.jpg',
...). For more details see the comments in runMFSF.m

The code also supports the usage of sparse landmarks to guide the
optical flow computation. In this case, there should be an ASCII
file per frame with the corresponding landmarks. Each file should
contain (# of landmarks rows), with each row having the numerical
values of X (horizontal) and Y (vertical with 0 at the top left)
coordinates (in pixels) of the corresponding landmark. There can be
other rows too, which will be ignored, but they should not start
with a number. For more details see the comments in runMFSF.m and
the function tools/read_landmarks.m. Like in the case of image
files, a consistent naming format should be used, e.g.
'image_%04d.pts'. For examples of such files, see the .pts files
within 'in_test_landm' directory.


SYSTEM REQUIREMENTS & COMPILATION
---------------------------------

You need to have MATLAB installed as well as an NVIDIA GPU card in
your system. The code has been tested in Linux, Windows and MAC
operating systems.

The cuda files are in the directory 'cudafiles'. This directory
also contains the compiled ptx files. It is probable that the code
will work directly in your system, using the existing ptx files.
However, if these files do not work in your system, you would have
to re-compile the .cu files. You can test if the code runs directly
by running the demo function demo.m, by typing demo(1) in the
command window of MATLAB.

Note that the existing ptx files were compiled with cuda 5.0 and
have been tested and working with NVIDIA GTX 780 on ubuntu 14.04,
as well as with machines running Windows and MAC.

Creating new ptx files (if needed) can be done by re-compiling the
cuda kernels, which requires a suitable cuda compiler driver (NVCC)
to be installed on your machine. Please see:
http://docs.nvidia.com/cuda/cuda-compiler-driver-nvcc/#axzz3Z9uSuftn
for details on cuda setup.

Once you have NVIDIA drivers with the compatible cuda toolkit
installed (with nvcc in your path) you can compile the files on
bash with (this is for the case of Linux, but similar procedure can
be followed in other OS):

$ cd cudafiles
$ nvcc -ptx *.cu


NOTE
----

Note that this code serves as a prototype implementation of the
MFSF method, without the main focus being the computational
efficiency. Note also that most of the results reported in the
above article cannot be replicated by simply running this code with
default parameters, but by choosing specific values for several
parameters. If you need to replicate these results, please contact
the authors.


LINKS & CONTACT DETAILS
-----------------------

More details, data and visualisations of results generated with
this code can be found on the webpage:
http://www.cs.ucl.ac.uk/staff/lagapito/subspace_flow/

This code is publicly available on Bitbucket:
https://bitbucket.org/troussos/mfsf/downloads

The code has been written by Ravi Garg and Anastasios Roussos.
Please contact them for any comments or questions:

Ravi Garg: ravi.garg@adelaide.edu.au

Anastasios Roussos: troussos@imperial.ac.uk (www.troussos.gr)


LICENSE
-------

All the scripts contained in this distributions are Free Software
under the GNU General Public License, version 3, June 2007

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
