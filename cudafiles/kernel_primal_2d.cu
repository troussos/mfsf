__global__ void regulariser_primal(float* L, const float* pL_x, const float *pL_y, const int nframe, const int RANK, const int w, const int h, const int d, const float theta)
{

	unsigned int  aa = blockIdx.x * blockDim.x + threadIdx.x;
    unsigned int  bb = blockIdx.y * blockDim.y + threadIdx.y;
    //unsigned int  cc = blockIdx.z * blockDim.z + threadIdx.z;
    unsigned int  spatio_temp_point = aa + (gridDim.x*blockDim.x)*bb; //+ gridDim.z*cc;
    //unsigned int  spatio_temp_point = aa + 16*bb;
    if(spatio_temp_point < w*h*d){
    unsigned int z = spatio_temp_point/(w*h);
    unsigned int y = (spatio_temp_point - z*w*h)/(w);
    unsigned int x = (spatio_temp_point - z*w*h - y*w);

    float pL_x_xneigh = 0;
    float pL_y_yneigh = 0;
    if (x >= 1)
        pL_x_xneigh = pL_x[(x-1) + y*w + z*w*h];
    if (y >= 1)
        pL_y_yneigh = pL_y[x + (y-1)*w + z*w*h];
        

    float div_L = pL_x[spatio_temp_point]-pL_x_xneigh  +   pL_y[spatio_temp_point]-pL_y_yneigh;//dxm(pL{r}(:,:,1)) + dym(pL{r}(:,:,2));

    L[spatio_temp_point] = L[spatio_temp_point] + theta*div_L;
    }
}

/* Wrong boundy conditions
__global__ void regulariser_primal(float* L, const float* pL_x, const float *pL_y, const int nframe, const int RANK, const int w, const int h, const int d, const int lambda, const float theta)
{

	unsigned int  aa = blockIdx.x * blockDim.x + threadIdx.x;
    unsigned int  bb = blockIdx.y * blockDim.y + threadIdx.y;
    //unsigned int  cc = blockIdx.z * blockDim.z + threadIdx.z;
    unsigned int  spatio_temp_point = aa + (gridDim.x*blockDim.x)*bb; //+ gridDim.z*cc;
    //unsigned int  spatio_temp_point = aa + 16*bb;
    if(spatio_temp_point < w*h*d){
    unsigned int z = spatio_temp_point/(w*h);
    unsigned int y = (spatio_temp_point - z*w*h)/(w);
    unsigned int x = (spatio_temp_point - z*w*h - y*w);

    unsigned int x_neigh = (max(x,1)-1) + y*w + z*w*h;//x-1+ (y)*w + z*w*h
    unsigned int y_neigh = x + (max(y,1)-1)*w + z*w*h;
    float div_L = pL_x[spatio_temp_point]-pL_x[x_neigh]  +   pL_y[spatio_temp_point]-pL_y[y_neigh];//dxm(pL{r}(:,:,1)) + dym(pL{r}(:,:,2));

    L[spatio_temp_point] = L[spatio_temp_point] + theta*div_L;
    }
}
*/

/*__global__ void kernel(const float* matrix, float* matrix2, int p, int q, int r, int c)
{
        //unsigned int x = blockIdx.x * blockDim.x + threadIdx.x;
        //unsigned int y = blockIdx.y * blockDim.y + threadIdx.y;
        //unsigned int z = blockIdx.z * blockDim.z + threadIdx.z;
        //unsigned  int spatio_temp_point = x+ y* p+ z*p*q;
        unsigned  int spatio_temp_point = blockIdx.x * blockDim.x + threadIdx.x;
        unsigned int z= spatio_temp_point/(p*q);
        unsigned int y= (spatio_temp_point - z*p*q)/(p);
        unsigned int x= (spatio_temp_point - z*p*q - y*p);
        
        unsigned int x_neigh = (max(x,1)-1) + y*p + z*p*q;//x-1+ (y)*w + z*w*h
        unsigned int y_neigh = x + (max(y,1)-1)*p + z*p*q;
        unsigned int z_neigh = x + y*p + (max(z,1)-1)*p*q;
        
        matrix2[spatio_temp_point] = matrix[spatio_temp_point] - matrix[z_neigh];

}*/

