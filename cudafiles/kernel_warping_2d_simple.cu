__global__ void warp(float* I_warp, const float *I, const float* u, const float* v, int w, int h, int d)
{

        unsigned int  aa = blockIdx.x * blockDim.x + threadIdx.x;
        unsigned int  bb = blockIdx.y * blockDim.y + threadIdx.y;
        //unsigned int  cc = blockIdx.z * blockDim.z + threadIdx.z;
        unsigned int  spatio_temp_point = aa + (gridDim.x*blockDim.x)*bb; //+ gridDim.z*cc;
        
        if(spatio_temp_point < w*h*d)
        {
                unsigned int z = (spatio_temp_point/(w*h));
                unsigned int y = ((spatio_temp_point - z*w*h)/(w));
                unsigned int x = ((spatio_temp_point - z*w*h - y*w));
		
                float xx = float(x) + u[spatio_temp_point]; 
                float yy = float(y) + v[spatio_temp_point];
                //float zz = (float)z;
		
                //computing intencity of x neigh
                int x1 = floorf(xx); //max((int)floorf(xx),0);//floorf(xx) >= 0 ? floorf(xx) : 0;
                int x2 = x1 + 1;  //min((int)ceilf(xx),w-1);//ceilf(xx) < h ? ceilf(xx) : h;
		
                //computing intencity of y neigh
                int y1 = floorf(yy); //max((int)floorf(yy),0);//floorf(yy) >= 0 ? floorf(yy) : 0;
                int y2 = y1 + 1;  //min((int)ceilf(yy),h-1);//ceilf(yy) < w ? ceilf(yy) : w;
		
                if(x1 < 0 || x1 > w-1 || y1 < 0 || y1 > h-1 || x2 < 0 || x2 > w-1 || y2 < 0 || y2 > h-1|| z>d-1 )
                //if(x1 < 0 || x2 > w-1 || y1 < 0 || y2 > h-1 || z>d-1 )
                    I_warp[spatio_temp_point] = 0;
                else
                {
        
                    float I_in_x_y1, I_in_x_y2, I_in_xy;
                    I_in_x_y1 = (float(x2)-xx) * I[x1 + y1 * w + z*w*h] +  (xx-(float)x1) * I[x2 + y1 * w + z*w*h];// I_in_x_y1 /= (x2-x1);
                    I_in_x_y2 = (float(x2)-xx) * I[x1 + y2 * w + z*w*h] +  (xx-(float)x1) * I[x2 + y2 * w + z*w*h];// I_in_x_y2 /= (x2-x1);
                    
                    I_in_xy = ((float)y2-yy) * I_in_x_y1 + (yy-(float)y1) * I_in_x_y2; //I_in_xy /= (y2-y1);
                    
                    I_warp[spatio_temp_point] = I_in_xy;
        
                }
	}
}


