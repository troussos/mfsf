
__global__ void med_filter(float* Mf, const float* M, int w, int h, int d)
{
    const int WIN_WIDTH = 9 ;
	unsigned int  aa = blockIdx.x * blockDim.x + threadIdx.x;
	unsigned int  bb = blockIdx.y * blockDim.y + threadIdx.y;
	//unsigned int  cc = blockIdx.z * blockDim.z + threadIdx.z;
	unsigned int  spatio_temp_point = aa + (gridDim.x*blockDim.x)*bb; //+ gridDim.z*cc;

	if(spatio_temp_point < w*h*d)
	{
		int z = spatio_temp_point/(w*h);
		int y = (spatio_temp_point - z*w*h)/(w);
		int x = (spatio_temp_point - z*w*h - y*w);
		//read neigh to sort
		
		int WIN_SIZE = (sqrtf(WIN_WIDTH) - 1)/2;
		float a[WIN_WIDTH]; // (float*) malloc(size*sizeof(float));
		int count = 0;
		for(int ii = -WIN_SIZE ; ii<= WIN_SIZE;ii++)
			for(int jj = -WIN_SIZE; jj<= WIN_SIZE;jj++ )
			{
				int X = x+ii;
                int Y = y+jj;
                if(X < 0 )
                    X = -X -1;
                else if(X>=w)
                    X = 2*w -X -1;

                if(Y < 0 )
                    Y = -Y -1;
                else if(Y>=h)
                    Y = 2*h -Y -1;
         


                /*if( x+ii>=0 && x+ii<w)
                    X = x+ii;
                else
                   X = x-ii;// X = x-ii;
                if( y+jj>=0 && y+jj<h)
                    Y = y+jj;
                else
                    Y = y-jj;//Y = y-jj; 
                */
				a[count++] = M[(X) + (Y)*w + (z)*w*h ];
			}
		
		// sort
		
		for(int ii = 0; ii < WIN_WIDTH; ii++)
			for(int jj = ii; jj < WIN_WIDTH; jj++)
			{
				if(a[ii] > a[jj] )
				{
					float temp = a[jj];
					a[jj] = a[ii];
					a[ii] = temp;
				}
			}
		Mf[spatio_temp_point] = a[(WIN_WIDTH - 1)/2];
		
	}
}
/*

__global__ void med_filter(float* Mf, const float* M, int w, int h, int d)
{
    const int WIN_WIDTH = 9 ;
	unsigned int  aa = blockIdx.x * blockDim.x + threadIdx.x;
	unsigned int  bb = blockIdx.y * blockDim.y + threadIdx.y;
	//unsigned int  cc = blockIdx.z * blockDim.z + threadIdx.z;
	unsigned int  spatio_temp_point = aa + (gridDim.x*blockDim.x)*bb; //+ gridDim.z*cc;
	//unsigned int  spatio_temp_point = aa + 16*bb;
	if(spatio_temp_point < w*h*d)
	{
		int z = spatio_temp_point/(w*h);
		int y = (spatio_temp_point - z*w*h)/(w);
		int x = (spatio_temp_point - z*w*h - y*w);
		//read neigh to sort
		
		int WIN_SIZE = (sqrtf(WIN_WIDTH) - 1)/2;
		float a[WIN_WIDTH]; // (float*) malloc(size*sizeof(float));
		int count = 0;
		for(int ii = -WIN_SIZE ; ii<= WIN_SIZE;ii++)
			for(int jj = -WIN_SIZE; jj<= WIN_SIZE;jj++ )
			{
				if( x+ii>=0 && x+ii<w &&  y+jj>=0 && y+jj<h)
					a[count++] = M[(x+ii) + (y+jj)*w + (z)*w*h ];
				else
					a[count++] = 0;//M[(x) + (y)*w + (z)*w*h ];//0;
			}
		
		// sort
		
		for(int ii = 0; ii < WIN_WIDTH; ii++)//for(int ii = 0; ii < WIN_WIDTH; ii++)
			for(int jj = ii; jj < WIN_WIDTH; jj++)//for(int jj = ii; jj < WIN_WIDTH; jj++)
			{
				if(a[ii] > a[jj] )
				{
					float temp = a[jj];
					a[jj] = a[ii];
					a[ii] = temp;
				}
			}
		Mf[spatio_temp_point] = a[(WIN_WIDTH - 1)/2];
		
	}
}*/