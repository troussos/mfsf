__global__ void thresholding(const float* I_x, const float* I_y, const float* I_t,  float* u, float* v, const float* u0, const float* v0, float* lambda, float theta, int w, int h, int d)
{
    
    unsigned int  aa = blockIdx.x * blockDim.x + threadIdx.x;
    unsigned int  bb = blockIdx.y * blockDim.y + threadIdx.y;
    //unsigned int  cc = blockIdx.z * blockDim.z + threadIdx.z;
    unsigned int  spatio_temp_point = aa + (gridDim.x*blockDim.x)*bb; //+ gridDim.z*cc;
    //unsigned int  spatio_temp_point = aa + 16*bb;
    if(spatio_temp_point < w*h*d)
    {
        unsigned int z = spatio_temp_point/(w*h);
//        unsigned int y = (spatio_temp_point - z*w*h)/(w);
//        unsigned int x = (spatio_temp_point - z*w*h - y*w);
        float r = I_t[spatio_temp_point] + (u[spatio_temp_point]-u0[spatio_temp_point])*I_x[spatio_temp_point] + (v[spatio_temp_point]-v0[spatio_temp_point])*I_y[spatio_temp_point];
        float I_grad_sq = max(.000001, I_x[spatio_temp_point]*I_x[spatio_temp_point] + I_y[spatio_temp_point]*I_y[spatio_temp_point]);
        float comp_var = lambda[z]*theta*I_grad_sq;
        if( r < -comp_var)
        {
            u[spatio_temp_point] += lambda[z]*theta*I_x[spatio_temp_point];
            v[spatio_temp_point] += lambda[z]*theta*I_y[spatio_temp_point];
        }
    
        if( r > comp_var)
        {
            u[spatio_temp_point] -= lambda[z]*theta*I_x[spatio_temp_point];
            v[spatio_temp_point] -= lambda[z]*theta*I_y[spatio_temp_point];
        }
    
        if( r <= lambda[z]*theta*I_grad_sq && r >= -lambda[z]*theta*I_grad_sq)
        {
            u[spatio_temp_point] -= r/I_grad_sq*I_x[spatio_temp_point];
            v[spatio_temp_point] -= r/I_grad_sq*I_y[spatio_temp_point]; 
        }
    }
}

