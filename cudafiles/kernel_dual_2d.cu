__global__ void regulariser_dual(const float* L, float* pL_x, float *pL_y, const float* g, const int nframe, const int RANK, const int w, const int h, const int d, const float theta, const float tau, const float epsilon)
{
/* Need so many variables.
No need of texture
3D cudaArrays of L~ and L needed
*/

    
	unsigned int  aa = blockIdx.x * blockDim.x + threadIdx.x;
    unsigned int  bb = blockIdx.y * blockDim.y + threadIdx.y;
    //unsigned int  cc = blockIdx.z * blockDim.z + threadIdx.z;
    unsigned int  spatio_temp_point = aa + (gridDim.x*blockDim.x)*bb; //+ gridDim.z*cc;
    //unsigned int  spatio_temp_point = aa + 16*bb;
    if(spatio_temp_point < w*h*d)
    {
    unsigned int z = spatio_temp_point/(w*h);
    unsigned int y = (spatio_temp_point - z*w*h)/(w);
    unsigned int x = (spatio_temp_point - z*w*h - y*w);
	int x_neigh = min(x+1,w-1)+ y*w + z*w*h;
    int y_neigh = x+ min(y+1,h-1)*w + z*w*h;


//compute derivatives
	float L_x = L[x_neigh] - L[spatio_temp_point] ;//= dxp(L{r});
	float L_y = L[y_neigh] - L[spatio_temp_point];//dyp(L{r});
  

// update dual variable
	pL_x[spatio_temp_point] = pL_x[spatio_temp_point] + tau*(L_x - epsilon*pL_x[spatio_temp_point]);
	pL_y[spatio_temp_point] = pL_y[spatio_temp_point] + tau*(L_y - epsilon*pL_y[spatio_temp_point]);
  
  
// reprojection to |pu| <= 1
	float length = sqrt(pL_x[spatio_temp_point]*pL_x[spatio_temp_point] + pL_y[spatio_temp_point]*pL_y[spatio_temp_point])/g[x+ (y)*w];
	float reprojection = max(1.0f, length);
	pL_x[spatio_temp_point] = pL_x[spatio_temp_point]/reprojection;
	pL_y[spatio_temp_point] = pL_y[spatio_temp_point]/reprojection;
//see what the hell is g a gaussian blur of imagesize or something!!!
    }
}

