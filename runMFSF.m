function [u,v,parmsOF,info] = runMFSF(varargin)
% Run Multiframe Subspace Flow. Implementation of the method of the
% following article:
%
% R. Garg, A. Roussos, L. Agapito,
% "A Variational Approach to Video Registration with Subspace Constraints",
% International Journal of Computer Vision (IJCV), vol. 104, issue 3, pp.
% 286-314, Sept. 2013.
%
% If you use this code for a scientific publication, please reference the
% above paper.
%
% This code implements the estimation of multiframe optical flow by
% minimising the energy proposed on the above article (Equation (6) of the
% article), following a coarse-to-fine technique with multiple warping
% iterations. Furthermore, it supports the following 2 additional features
% (not described in the article): 
%
% 1) The code supports the additional option of adding a landmarks term on
% that energy:
%
% beta_lndm*Sum_{i=1}^{N_lndm} Sum_{n=1}^{F} ||u(x_i;n) - u_lndm(x_i;n)||^2 ,
% where N_lndm is the number of landmarks, x_i are their 2D coordinates in
% the reference frame and u_lndm(x_i;n) is the 2D displacement of the i-th
% landmark in the n-th frame, wrt its position in the reference frame. This
% serves as quadratic prior in the energy that would drag the solution
% towards the landmarks.
%
% 2) The code supports the option of incorporating in the data term the
% gradient maps of the image channel(s) input frames (gradient constancy
% assumption), either in combination with the actual channels or by
% themselves. This adds robustness to specularities and other illumination
% changes of the scene.
%
%
% Syntax for calling this function: 
% [u,v,parmsOF,info] = runMFSF('[variable1_name]',[variable1_value],'[variable2_name]',[variable2_value],...),
% where [variable*_name] can be any variable from the following list (if not specified the
% default value will be used) and [variable*_value] is the corresponding value:
%
% % % % Variables that define the input sequence:
%
% path_in: path with the input sequence frames.
%
% frname_frmt: format of the input frame names, to be used with sprintf and
% the frame number as argument. e.g. 'frame%04d.pgm'.
%
% sframe: starting frame of the sequence to be processed [default: 1].
%
% nframe: number of frames. [default: 5].
%
% step_frame: sampling step for the frames. This means that the sequence of
% frames that will be used will be (sframe, sframe+step_frame, ...,
% sframe+(nframe-1)*step_frame). [default: 1].
%
% nref: reference frame (not necessarily in the list of frames that are
% used). [default: 1].
%
% % % % Variables that define parameters of the method
% % % % (NOTE: Even default parameters should typically work well. If you need
% % % % to improve the result, tuning only alpha and flag_grad should be
% % % % sufficient. Also, in case you use landmarks, you might want to
% % % % decrease beta_lndm if these are not tracked reliably enough. Finally,
% % % % change MaxPIXpyr if you have memory issues):
%
% alpha: weight of the photo-consistency data term. As it gets larger, the
% solution becomes less regularised [default: 30].
%
% flag_grad: flag that determines whether the gradient maps of the input
% frames will be used in the data term:
%   - if 0: no gradient will be used, the data term will consist of the
%   channels of the frames (intensities or colors of the frames, after
%   potential structure texture decomposition).
%   - if 1: the gradient of all channels will be used in combination with the actual image channels. 
%   - if 2: only the gradient of all channels will be used in the data
%   term. Compared to the choice 0, this increases the robustness to
%   illumination changes (but in the same time requires more memory and
%   runs slower).
% [default: flag_grad=2].
%
% bas: parameter specifying the basis: it can be an array or a string:
%   - if bas is an array it should be 2nframes x R, where R is the rank of the basis. In this case, this
%   array is the trajectory basis that you want to use. bas should be orthonormal, i.e. bas'*bas
%   should equal the identity matrix. Every column of bas should contain a
%   different basis element in the format [u(1) ... u(nframe) v(1) ...
%   v(nframe)]^T, where u and v are the horizontal and vertical components
%   of the displacement respectively. This means that the first nframes
%   rows of bas correspond to the horizontal displacement and the last
%   nfrmaes rows to the vertical displacement.
%   - if bas is a string, then it should specify a type of commonly used
%   basis. It can have the values: 'DCT', 'CubSplines' or 'EYE' (for
%   identity matrix).
% [default: bas='DCT'].
%
% R (used only if bas is specified as a string): RANK of the basis. If R=inf
% or R>2*nframe, then the used R will be R=2*nframe (full rank). [default: Inf].
%
% param_refPCAonRes: parameter about refining result by rerunning method
% using a PCA basis computed on the result:
%   - If NaN: do not do any such refinement
%   - If numeric value (it should be in (0,100]): percentage of the
%   variance of the computed dense 2D tracks that you'd like to be
%   explained by the PCA basis. This will determine the Rank of the PCA
%   basis used in refinement (the rank that achieves a percentage closest
%   to the one specified will be used).
% [default: param_refPCAonRes=NaN].
%
% beta: weight of the quadratic coupling term of the energy, which links
% the multiframe optical flow that lies in the subspace (determined by
% L(x)) with the flow that tries to explain the observations (u(x;n)).
% [default: 5].
%
% Cg: parameter for the space-varying weight g(x) of the regularisation
% term of the energy that acts as an edge-weighting. More specifically the
% coefficient of the squared norm of the gradient as described by the
% equation:
% g(x) = exp(-Cg |grad(conv(G_SIGg(x),I(x;n0)))|^2 ).
% Set to 0 if you don't want to use such space varying weight g(x), i.e.
% you want g(x)=1 everywhere. [default: 1].
%
% SIGg (used only if Cg~=0): sigma_g, standard deviation (in pixels) of the 2D Gaussian
% G_SIGg(x) that convolves the reference image I(x; n0) in the definition
% of the regularisation weight g(x):
% g(x) = exp(-Cg |grad(conv(G_SIGg(x),I(x;n0)))|^2 ).
% [default: SIGg=1].
%
% TVeps: constant epsilon for the regularisation of the linear model
% coefficients L_i(x), that defines the transition from quadratic
% regularisation (for very small values of the gradient) to Total Variation
% regularisation. [default: .1].
%
% PF: pyramid_factor (in (0,1)), i.e. factor to downsample the image of
% every pyramid level in order to proceed to the next level, within the
% coarse-to-fine strategy. [default: .95].
%
% MaxPIXpyr: maximum resolution (number of pixels) of a pyramid level, i.e.
% finest scale, that will be processed. If original image resolution is
% higher, then the final result will just be a bilinear interpolation of
% the result from the pyramid level with resolution MaxPIXpyr pixels.
% Otherwise, this parameter has no effect in the creation of the image
% pyramids. [default: Inf].
%
% PMD: minimum dimension of a pyramid level, i.e. width and height of even the
% coarsest pyramid level must be greater or equal to that parameter PMD. If
% this is too large, then then only the pyramid with MaxPIXpyr (or full
% resolution) will be used. [default: 8].
%
% MaxPL: max number of pyramid levels that will be processed (starting
% counting from pyramid with MaxPIXpyr resolution (or full image resolution
% if this deosn't apply) and then continuing counting the pyramids with
% lower resolution). Depending on the values of PF,MaxPIXpyr and PMD, the
% actual pyramid levels that will be processed could be much fewer. For
% fast testing if the code is running and how much memory it needs you can set
% this value to 1 (but expect dummy results). [default: 1000].
%
% W: number of Warping iterations per pyramid level. For fast code testing
% as above, you can set this to 1. [default: 8].
%
% IPW: number of alternation Iterations Per Warp (alternation loop within
% each warping iteration). [default: 10].
%
% STDfl: flag that defines the type of Structure Texture Decomposition to be done as
% preprocessing:
%   STDfl=0: do not use any structure texture decomposition.
%   STDfl=1: use structure texture decomposition.
% [default: STDfl=1].
%
% STDsc (used only if STDfl=1): scale of the ST decomp: this defines the std
% dev of the gaussian filter to be applied. [default: 1].
%
% STDfac (used only if STDfl=1): multiplying coefficient for STD, to derive
% the final textural part of the image. [default: .8].
%
% % % % Parameters, in case you want to use landmarks to guide the optical
% % % % flow computation:
%
% lndm_fname_frmt: If empty string '', then no landmarks will be used.
% Otherwise this should be the format of the file names 
% with the landmarks for every frame, to be used with sprintf and the frame
% number as argument. e.g. 'image_%04d.pts'. Each file should be of ASCII
% format containing (# of landmarks rows), with each row
% having the numerical values of X (horizontal) and Y (vertical with 0 at
% the top left) coordinates (in pixels) of the corresponding landmark.
% There can be other rows too but they should not start with a number 
% and will be ignored (to be read by read_landmarks function). [default: ''].
%
% beta_lndm (used only if lndm_fname_frmt non-empty):
% weight of the landmarks term, for points with available landmarks. As it
% gets larger, the multiframe optical flow is more strongly constrained to
% follow the motion of the landmarks in the corresponding sparse points.
% Its range is directly comparable to the parameter beta. [default: 50].
%
% path_lnmd (used only if lndm_fname_frmt non-empty): path with the files
% that contain the landmarks. If not specified or '' then path_lnmd will be
% set to path_in, which means that these files should be in path_in.
% [default: ''].
%
%
% % % % % % OUTPUT VARIABLES:
% u,v: Nx x Ny x Nframes arrays with X and Y coordinates of the flow for
% every frame.
%
% parmsOF: structure with all the parameters that have been used for this
% result (specified by input or default ones, not specified).
%
% info: structure with some additional information about the result.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

addpath('cudafiles');
addpath('tools');

%logical to indicate if we want to normalise the images between 0 and -1
%(hard coded):
flag_normlz = false;

%%%%%%%%%%% Parse the inputs:
p = inputParser;    % Create an instance of the class


p.addParamValue('path_in', '',    @(x)ischar(x));
p.addParamValue('frname_frmt', '',    @(x)ischar(x));

p.addParamValue('nref', 1,   @(x)isnumeric(x)&&round(x)==x&&x>0);
p.addParamValue('sframe', 1,   @(x)isnumeric(x)&&round(x)==x&&x>0);
p.addParamValue('nframe', 5,   @(x)isnumeric(x)&&round(x)==x&&x>0);
p.addParamValue('step_frame', 1,   @(x)isnumeric(x)&&round(x)==x&&x>0);

p.addParamValue('bas', 'DCT');
p.addParamValue('param_refPCAonRes', NaN,    @(x)isnumeric(x));

p.addParamValue('beta', 5,    @(x)isnumeric(x)&&x>0);
p.addParamValue('alpha', 30,       @(x)isnumeric(x) && x>0);
p.addParamValue('R',inf,         @(x)isnumeric(x)&&round(x)==x&&x>0);

p.addParamValue('Cg',1,         @(x)isnumeric(x) && x>=0);
p.addParamValue('SIGg',1,        @(x)isnumeric(x) && x>0);
p.addParamValue('PF',.95,       @(x) (isnumeric(x) && 0<x && x<1 ) );
p.addParamValue('PMD',8,       @(x) isnumeric(x) && x>0 ); %%%%% see if it is 8 or 16 %%%%%%%
p.addParamValue('MaxPIXpyr',Inf,       @(x)isnumeric(x)&&x>0);
p.addParamValue('MaxPL',1000,    @(x)isnumeric(x)&&round(x)==x&&x>0);


p.addParamValue('W',5,         @(x)isnumeric(x)&&round(x)==x&&x>0);
p.addParamValue('IPW',10,       @(x)isnumeric(x)&&round(x)==x&&x>0);
p.addParamValue('STDfl',1,      @(x)isnumeric(x)&&(x==0||x==1));

p.addParamValue('STDsc',1,   @(x)isnumeric(x)&&x>0);
p.addParamValue('STDfac',.8,     @(x)isnumeric(x)&&x>0);
p.addParamValue('TVeps',.1,     @(x)isnumeric(x)&&x>0);

p.addParamValue('flag_grad',2,    @(x)isnumeric(x)&&(x==0||x==1||x==2));

p.addParamValue('lndm_fname_frmt', '',    @(x)ischar(x));
p.addParamValue('beta_lndm', 50, @(x)isnumeric(x)&&x>0);
p.addParamValue('path_lnmd', '',    @(x)ischar(x));

p.parse(varargin{:});

% struct with the params of the OF run:
parmsOF = p.Results;
clear p;

basis = parmsOF.bas;
param_refPCAonRes = parmsOF.param_refPCAonRes;


% in the rest of the code, the weight of the data term is denoted as lambda
% instead of alpha:
lambda = parmsOF.alpha; 
% in the rest of the code, we use the parameter theta that determines the
% weight of the quadratic coupling as 1/(2*theta):
theta = 1/(2*parmsOF.beta);


RANK = parmsOF.R;
% in the rest of the code, the coefficient Cg is denoted as
% paramsTVW.alpha:
paramsTVW.alpha = parmsOF.Cg;
% logical indicating if we will use edge information specify a
% space-varying weight g(x) for the regularisation:
paramsTVW.use_edges = parmsOF.Cg~=0;
paramsTVW.sigma = parmsOF.SIGg;
pyramid_factor = parmsOF.PF;
min_dim_pyramid = parmsOF.PMD;
MaxPIXpyr = parmsOF.MaxPIXpyr;
warps = parmsOF.W;
maxits = parmsOF.IPW;
paramsSTD.flag = parmsOF.STDfl;

paramsSTD.scale = parmsOF.STDsc;
paramsSTD.factor = parmsOF.STDfac;

epsilon = parmsOF.TVeps;
max_pyramid_levels = parmsOF.MaxPL;

flag_grad = parmsOF.flag_grad;

path_in = parmsOF.path_in;
frname_frmt = parmsOF.frname_frmt;

lndm_fname_frmt = parmsOF.lndm_fname_frmt;
% as with theta, in the rest of the code, we use the parameter theta_lndm that
% determines the weight of the landmarks term as 1/(2*theta_lndm):
theta_lndm = 1/(2*parmsOF.beta_lndm);
path_lnmd = parmsOF.path_lnmd;


nref = parmsOF.nref;
sframe = parmsOF.sframe;
nframe = parmsOF.nframe;
step_frame = parmsOF.step_frame;


%%%%%%%%%%%%%%%%%%%%%

% Define cuda kernels that will be used:
kmedf = parallel.gpu.CUDAKernel('kernel_med_filter_2d.ptx','kernel_med_filter_2d.cu');
kwarp = parallel.gpu.CUDAKernel('kernel_warping_2d_simple.ptx','kernel_warping_2d_simple.cu');

kt = parallel.gpu.CUDAKernel('kernel_thresholding_rescl_lambda.ptx','kernel_thresholding_rescl_lambda.cu');

k1 = parallel.gpu.CUDAKernel('kernel_primal_2d.ptx','kernel_primal_2d.cu');
k2 = parallel.gpu.CUDAKernel('kernel_dual_2d.ptx','kernel_dual_2d.cu');

global u_ v_ L ;


% separate treatment of I_ref to account for the possibility that the
% reference frame doesn't belong to the sequence (e.g. if nref<sframe):
ref_str = fullfile(path_in, sprintf(frname_frmt, nref));
% reference frame to be used in brightness constancy (eventually either
% original input or struct-text-decomposed version), in original
% finest scale:
I_ref_fs = single(imread_nrmlz(ref_str,flag_normlz,flag_grad,0));


[Nrows, Ncols, Nchans] = size(I_ref_fs);


% normalize lamda based on the number of channels:
parmsOF.alpha_beforeNorm = lambda;
lambda = lambda/sqrt(Nchans);
parmsOF.alpha = lambda;

for f = 1:nframe
    filename = fullfile(path_in, sprintf(frname_frmt, sframe+step_frame*(f-1)));
    
    % sequence of images to be used in brightness constancy (eventually either
    % original input or struct-text-decomposed version), in original
    % finest scale:
    
    I_fs(:,:,f,:) = single(imread_nrmlz(filename,flag_normlz,flag_grad,0));
    
end


use_landmarks = ~isempty(lndm_fname_frmt);
if use_landmarks
    
    if isempty(path_lnmd)
        % use same path as path with input frames:
        path_lnmd = path_in;
    end
    
    landmarks_ref = read_landmarks( fullfile(path_lnmd, sprintf(lndm_fname_frmt, nref)) );
        
    %  this is called to be saved in the output struct info (potentially
    %  usefull for grid visualisations of the result):
    [inds_act_lndm, inds_lndm2D] = extract_lndm_mask(landmarks_ref,Nrows,Ncols,nframe);  
        
    Nlndm = size(landmarks_ref,1);
    
    u_lndm = zeros(Nlndm,nframe);
    v_lndm = zeros(Nlndm,nframe);
    
    % this will contain the active landmarks (to be used by test figs
    % only):
    landmarks_seq = zeros(length(inds_act_lndm),2,nframe);
    for f = 1:nframe
        filename_lndm = fullfile(path_lnmd, sprintf(lndm_fname_frmt, sframe+step_frame*(f-1)));
                
        landmarks_f = read_landmarks( filename_lndm );
        
        u_lndm(:,f) = landmarks_f(:,1) - landmarks_ref(:,1);
        v_lndm(:,f) = landmarks_f(:,2) - landmarks_ref(:,2);
        
        % this is just to be returned as output:
        landmarks_seq(:,:,f) = landmarks_f(inds_act_lndm,:);
    end    
    
    u_lndm = gpuArray(u_lndm(:));
    v_lndm = gpuArray(v_lndm(:));
    

else    
    % a NaN value to theta_lndm will from now on mean that we have no
    % landmarks:
    theta_lndm = NaN;
    u_lndm = [];
    v_lndm = [];
    landmarks_ref = [];
    
    inds_lndm2D = [];
    landmarks_seq = [];    
end

% logical about using dualisation code for thresholding (used in case of
% multichannel images or when landmarks are used):
if size(I_fs,4)>1 || use_landmarks
    USE_DUAL_CC =1;
else
    USE_DUAL_CC = 0;
end

lambda_resc = lambda*ones(1,nframe);


% Create basis:
if ischar(basis)
    
    if RANK>2*nframe
        if ~isinf(parmsOF.R)
            warning('Specified Rank is bigger than its max value 2*nframe (full rank). I will replace specified Rank by 2*nframe.');
        end
        
        RANK = 2*nframe;
    end
    
    [~, basis_orth] = create_basis(basis,RANK,nframe,2);
    Qu = basis_orth(1:end/2,:);
    Qv = basis_orth(end/2+1:end,:);
    RANK = size(basis_orth,2);
    
else
                    
    RANK = size(basis,2);    
    
    if size(basis,1)~=2*nframe || RANK>2*nframe
       error('When specified as an array, the basis parameter bas should have dimensions 2nframe x R whith R<=2nframe.'); 
    end
    
    Qu = basis(1:nframe,:);
    Qv = basis(nframe+1:end,:);    
    
end


% labels to control the rescaling of L coefficients:
labls_Lresc = find_labls_Lresc(Qu,Qv);


if RANK > 2*nframe
    error('Actual rank should be less than the dimensionality of trajectories (i.e. 2*Nframes)');
end

if size(Qu,1) ~= nframe
    fprintf('Warning: reference frame is removed while computing the basis');
    nframe = nframe-1;
    if size(Qu,1) ~= nframe
        error('Error in reference frame removal');
    end
end



tic

if(nref >= sframe && nref < sframe+step_frame*(nframe-1)+1 )
    ind_ref = (nref-sframe)/step_frame + 1;
    if round(ind_ref)==ind_ref
        nforce0 = ind_ref;
    else
        nforce0 = [];
    end
else
    nforce0 = [];
end

% Calling the main part of the code: 
iterate_coarse_fine(I_fs,I_ref_fs, ...
    (Qu),(Qv), lambda_resc, theta, epsilon, paramsTVW, maxits, ...
    max_pyramid_levels, pyramid_factor,min_dim_pyramid, MaxPIXpyr, warps, ...
    paramsSTD, ...
    nframe,RANK, ...
    kmedf, kwarp, kt, k1,k2,labls_Lresc,...
    USE_DUAL_CC, nforce0, landmarks_ref, u_lndm, v_lndm, theta_lndm);


if isnan(param_refPCAonRes)
   
    % these variables are not applicable in this case, but set them to NaN
    % in order to save them in the mat file:
    RANK_refPCA = NaN;
    varexpl_refPCA = NaN;
    
else    
    % In this case we re-run the method with a basis built from the
    % solution:
    
    Lpflow = size(u_,3);
     
    sampling_factor = 1; % dummy variable
    for ii = 1:Lpflow
        u_temp_ii = u_(1:sampling_factor:end,1:sampling_factor:end,ii);
        v_temp_ii = v_(1:sampling_factor:end,1:sampling_factor:end,ii);
        
        if ii==1
            W = zeros(2*Lpflow,numel(u_temp_ii));
        end
        
        W(2*ii-1,:) = u_temp_ii(:);
        W(2*ii,:) = v_temp_ii(:);
    end
    clear u_temp_ii v_temp_ii;
    
    
    ind_ref = (nref-sframe)/step_frame + 1;
    
    if round(ind_ref)==ind_ref && 1<=ind_ref && ind_ref<=Lpflow
        
        W(2*ind_ref-1 : 2*ind_ref,: ) = 0; %placing zero at reference frame
        
    end
            
    % KEEP the principal components:
    [U,variances,~] = svd(W*W');
    clear W;
    variances = diag(variances);
    
    varexplained = cumsum(variances);
    % express variance explained as a percentage:
    varexplained = 100*varexplained/varexplained(end);
    
    [~,RANK_refPCA] = min(abs(varexplained-param_refPCAonRes));
    varexpl_refPCA = varexplained(RANK_refPCA);
        
    
    fprintf('\n***Application of PCA on the 2D trajectories estimated by applying the first step of MFSF:\nThe RANK of PCA basis will be %d (%g%% of the initial %d components that correspond to full rank)\n',RANK_refPCA,100*RANK_refPCA/length(variances), length(variances));
    fprintf('This explains %g%% of the variance of the initial MFSF solution (the closest possible to the input value of %g%%)\n',varexpl_refPCA,param_refPCAonRes);
    
    
    Q = U(:,1:RANK_refPCA);
    Qu = Q(1:2:end,:);
    Qv = Q(2:2:end,:);
        
    % Calling again the main part of the code: 
    iterate_coarse_fine(I_fs,I_ref_fs, ...
        (Qu),(Qv), lambda_resc, theta, epsilon, paramsTVW, maxits, ...
        max_pyramid_levels, pyramid_factor,min_dim_pyramid, MaxPIXpyr, warps, ...
        paramsSTD, ...
        nframe,RANK_refPCA, ...
        kmedf, kwarp, kt, k1,k2,labls_Lresc,...
        USE_DUAL_CC, nforce0, landmarks_ref, u_lndm, v_lndm, theta_lndm);
    
end


clear global u_;
clear global v_;
% u,v: Nx x Ny x Nframes arrays with X and Y coordinates of the flow for
% every frame:
u = gather( reshape(  (Qu * (reshape(L(:),Nrows*Ncols,[]))')' ,Nrows,Ncols,nframe) ) ;
v = gather( reshape(  (Qv * (reshape(L(:),Nrows*Ncols,[]))')' ,Nrows,Ncols,nframe) ) ;

clear global L;

if ~isempty(nforce0)
    % force flow at reference to be 0:
    u(:,:,nforce0) = 0;
    v(:,:,nforce0) = 0;
end


fprintf('\n\n**Running MFSF is DONE.\n');
runtime = toc

info.Qu = Qu;
info.Qv = Qv;
info.runtime = runtime;
info.RANK_refPCA = RANK_refPCA;
info.varexpl_refPCA = varexpl_refPCA;

% indices wrt the pixels of the ref frame (original resolution - finest
% scale) describing to the active landmarks   
info.inds_lndm2D = inds_lndm2D;
% Nlandmarks x 2 x nframe array with the landmarks that were active (within
% borders of ref and after removing potential duplicates) in the finest
% resolution: 
info.landmarks_seq = landmarks_seq;
