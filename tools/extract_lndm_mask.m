function [inds_act_lndm, inds_lndm2D, inds_act_lndm3D, inds_lndm3D] = extract_lndm_mask(landmarks,Nrows,Ncols,nframe)
% Extract mask of the landmarks, wrt the input images, as well as an index
% of active landmarks (in the sense that they are within the image domain
% in the reference).
%
% landmarks: Nlandms x 2 array with the X,Y (horizontal,vertical) coordinates of the landmarks at
% the ref image, in pixels
% Nx,Ny: size of the input images, corresponding to X and Y

% quantize landmarks coordinates to have only integer values (in pixels):
landmarks = round(landmarks);

Nlmdm_orig = size(landmarks,1);
% remove duplicates (in the case that two landmarks coincide in the ref
% frame) by just keeping the first landmark:
[landmarks, inds_unique_lndms] = unique(landmarks,'rows');


inds_act_lndm = find(1<=landmarks(:,2) & landmarks(:,2)<=Nrows & 1<=landmarks(:,1) & landmarks(:,1)<=Ncols);

% use only active landmarks:
landmarks = landmarks(inds_act_lndm,:);
Nlndm = size(landmarks,1);
% combine the two indexings:
inds_act_lndm = inds_unique_lndms(inds_act_lndm);

inds_lndm2D = sub2ind([Nrows,Ncols],landmarks(:,2),landmarks(:,1));

if nargout>2

    fprintf('\nCurrent pyramid level:\n%d out of %d landmarks are kept as non-duplicate, and',length(inds_unique_lndms),Nlmdm_orig);
    fprintf('\n%d out of %d landmarks are kept as both non-duplicate and within borders of ref image.\n',length(inds_act_lndm),Nlmdm_orig);    
    
    Npix = Nrows*Ncols;
    inds_lndm3D = zeros(Nlndm*nframe,1);
    inds_act_lndm3D = zeros(Nlndm*nframe,1);
    for f=1:nframe
        inds_lndm3D((f-1)*Nlndm+1 : f*Nlndm) = (f-1)*Npix + inds_lndm2D;
        
        inds_act_lndm3D((f-1)*Nlndm+1 : f*Nlndm) = (f-1)*Nlmdm_orig +inds_act_lndm;
    end
    
end
