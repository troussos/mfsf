
function iterate_warp(Qu,Qv, ...
                                          I, I_ref, lambda, theta, epsilon, ...
                                          maxits, string_pyrlev, warps, ...
                                          g,nframes,RANK, ...
                                          kmedf, kwarp, kt, k1,k2,...
                                          nforce0,qI,USE_DUAL_CC, inds_lndm3D, u_lndm, v_lndm, theta_lndm)
% Run multiplr warping iterations
                                                                         
global u_ v_ u0 v0;

[M N ~] = size(I);

idx = repmat([1:N], M,1);
idy = repmat([1:M]',1,N);
mask = [1 -8 0 8 -1]/12;

I_x = zeros(M,N,size(I,3),size(I,4),'single'); I_y = zeros(M,N,size(I,3),size(I,4),'single');
for ch = 1:size(I,4)
    parfor f= 1:size(I,3)        
        I_x(:,:,f,ch) = imfilter(I(:,:,f,ch), mask, 'replicate');
        I_y(:,:,f,ch) = imfilter(I(:,:,f,ch), mask', 'replicate');
    end
end

for ch = 1:size(I,4)
    I_ref_x(:,:,ch) = imfilter(I_ref(:,:,ch), mask, 'replicate');
    I_ref_y(:,:,ch) = imfilter(I_ref(:,:,ch), mask', 'replicate');
end


for i=1:warps
    
    % median filtering:
    u0 = gather(feval(kmedf,u_,u_, M, N, nframes));
    v0 = gather(feval(kmedf,v_,v_, M, N, nframes));
    
    I_warped = gpuArray(zeros(size(I),'single')); I_x_warped = gpuArray(zeros(size(I),'single')); I_y_warped = gpuArray(zeros(size(I),'single'));
    for ch = 1:size(I,4)
        I_warped(:,:,:,ch)   = (feval(kwarp, zeros(size(I(:,:,:,ch)),'single'), I(:,:,:,ch)  ,v0,u0, M, N, nframes));%I_warped = gather(I_warp_GPU);clear I_warp_GPU;
        I_x_warped(:,:,:,ch) = (feval(kwarp, zeros(size(I(:,:,:,ch)),'single'), I_x(:,:,:,ch),v0,u0, M, N, nframes));%I_x_warped = gather(I_x_warp_GPU);clear I_x_warp_GPU;
        I_y_warped(:,:,:,ch) = (feval(kwarp, zeros(size(I(:,:,:,ch)),'single'), I_y(:,:,:,ch),v0,u0, M, N, nframes));%I_y_warped = gather(I_y_warp_GPU);clear I_y_warp_GPU;
    end
    
    idxx = repmat(idx,[1,1,nframes]) + u0;
    idyy = repmat(idy,[1,1,nframes]) + v0;
    
    for ch = 1:size(I,4)
        I_x_el(:,:,:,ch) = gather(0.5*gpuArray(repmat(I_ref_x(:,:,ch),[1,1,nframes]) + I_x_warped(:,:,:,ch)));
        I_y_el(:,:,:,ch) = gather(0.5*gpuArray(repmat(I_ref_y(:,:,ch),[1,1,nframes]) + I_y_warped(:,:,:,ch)));
        I_t(:,:,:,ch) = gather(I_warped(:,:,:,ch) - gpuArray(repmat(I_ref(:,:,ch),[1,1,nframes])) );
        m(:,:,:,ch) = (idxx > N) | (idxx < 1) | (idyy > M) | (idyy < 1);
    end
    
    clear I_x_warped I_y_warped I_warped;
    
    I_x_el(m) = 0.0;I_y_el(m) = 0.0;I_t(m) = 0.0;
    
    fprintf('Running MFSF for coarse-to-fine iteration %s, warp iteration %d/%d ...\n', string_pyrlev, i,warps);
    
    
    % Solve MFSF for current image linearisation:
    solveMFSF_curImLin(Qu,Qv, I_x_el,  I_y_el,  I_t, ...
        g, ... 
        lambda, theta, epsilon, maxits,nframes,RANK,...
        kt, k1,k2, ...
        nforce0,qI,USE_DUAL_CC, inds_lndm3D, u_lndm, v_lndm, theta_lndm);        
    
end


end