function I = imread_nrmlz(imname,flag_norm,flag_grad,do_mkgray)
% Read an image and normalize it to the range [-1 1]
% and return a double 2D/3D array

if nargin<3
    do_mkgray = 0;
end

I = imread(imname);

if do_mkgray && size(I,3)==3
   I = rgb2gray(I); 
end

I = single(I);

ch = size(I,3);
if flag_grad
    for i = 1:ch
        [I_grad1, I_grad2] = gradient(I(:,:,ch));
        I(:,:,ch+2*i-1) =  I_grad1;
        I(:,:,ch+2*i) = I_grad2;
    end
end

if flag_norm
    % normalize to the range [-1,1]:
    Imin = min(I(:));Imax = max(I(:));
    I = 2*(I - Imin)/(Imax - Imin) - 1;
else
    I = I/255;
end