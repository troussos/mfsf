function iterate_coarse_fine(I,I_ref_fs, ... 
                                 Qu,Qv, lambda, theta, epsilon, paramsTVW, maxits, ...
                                 max_pyramid_levels, pyramid_factor, min_dim_pyramid, MaxPIXpyr, warps, ...
                                 paramsSTD, ...
                                 nframe,RANK,...
                                 kmedf, kwarp, kt, k1,k2, labls_Lresc,...
                                 USE_DUAL_CC, nforce0, landmarks_ref, u_lndm, v_lndm, theta_lndm)
% Run coarse to fine iterations for the computation of MFSF

% parameter for the Grid Size of cuda kernels (fixed):
gSize_param = 16;                         
                             
flagSTdec = paramsSTD.flag;
                             
global u_ v_ L pLx pLy; 
                             
                             
[M N ~] = size(I);
width_Pyramid = cell(1,1);
height_Pyramid = cell(1,1);

% % % % Calculate image sizes for each pyramid level:
pyramid_levels = max_pyramid_levels;
width_Pyramid{1} = N;
height_Pyramid{1} = M;

if M*N <= MaxPIXpyr
    beg_pyr_level = 1;
else
    i = 2;
    while true
        width_Pyramid{i} = floor(pyramid_factor*width_Pyramid{i-1});
        height_Pyramid{i} = floor(pyramid_factor*height_Pyramid{i-1});    
        
        if width_Pyramid{i}*height_Pyramid{i} <= MaxPIXpyr
            % pyramid_factor<1, therefore
            % width_Pyramid{i}<width_Pyramid{i-1}, which means that this
            % will definetely happen at some point
            beg_pyr_level = i;
            break;
        end        
        
        i = i + 1;
    end
    
end

i = beg_pyr_level;
while i<beg_pyr_level+pyramid_levels-1
    
    i = i + 1;
    
    width_Pyramid{i} = floor(pyramid_factor*width_Pyramid{i-1});
    height_Pyramid{i} = floor(pyramid_factor*height_Pyramid{i-1});
    
    if min(width_Pyramid{i}, height_Pyramid{i}) < min_dim_pyramid;
        pyramid_levels = i-beg_pyr_level+1;
        break;
    end
    
    
end

end_pyr_level = beg_pyr_level+pyramid_levels-1;

% start coarse to fine iterations:
for level = end_pyr_level:-1:beg_pyr_level;
  clear I_Pyramid_i I_Pyramid_ref;

  
  for f =1:size(I,3)
      for ch =  1:size(I,4)
        I_Pyramid_i(:,:,f,ch) =  structure_texture_decomposition( imresize(I(:,:,f,ch) , [height_Pyramid{level} width_Pyramid{level}], 'bilinear'), flagSTdec, paramsSTD);
      end
  end

  
  for ch =  1:size(I_ref_fs,3)
      I_Pyramid_ref(:,:,ch) =  structure_texture_decomposition( imresize(I_ref_fs(:,:,ch) , [height_Pyramid{level} width_Pyramid{level}], 'bilinear'), flagSTdec, paramsSTD);
  end
    

  if paramsTVW.use_edges == 1
      g_Pyramid_i = compute_edge_weight(imresize(I_ref_fs, [height_Pyramid{level} width_Pyramid{level}], 'bilinear'), paramsTVW);
  else
      g_Pyramid_i = ones(height_Pyramid{level},width_Pyramid{level});
  end
  
  
  M = height_Pyramid{level};
  N = width_Pyramid{level};
  
  
    
  % % % Define the specifications of the cuda kernels for the current
  % % % pyramid level:

  size_1d = M*N*double(nframe);
  gSize = ceil(sqrt(size_1d/(gSize_param*gSize_param)));
  nblock = ceil(size_1d/(gSize^2));
  bSize_x = ceil(sqrt(nblock));
  bSize_y = ceil(nblock/bSize_x);  
  
  kmedf.GridSize = [gSize gSize];
  kmedf.ThreadBlockSize = [bSize_x bSize_y 1];
  
  kwarp.GridSize = [gSize gSize];
  kwarp.ThreadBlockSize = [bSize_x bSize_y 1];
  
  kt.GridSize = [gSize gSize];
  kt.ThreadBlockSize = [bSize_x bSize_y 1];

  
  size_1d = N*M*double(RANK);
  gSize = ceil(sqrt(size_1d/(gSize_param*gSize_param))); 
  nblock = ceil(size_1d/(gSize^2));
  bSize_x = ceil(sqrt(nblock));
  bSize_y = ceil(nblock/bSize_x);
    
  k1.GridSize = [gSize gSize];
  k1.ThreadBlockSize = [bSize_x bSize_y 1];
    
  k2.GridSize = [gSize gSize];
  k2.ThreadBlockSize = [bSize_x bSize_y 1];
    
  % % %
  % % %
  
  
  if level == end_pyr_level
 
    u_ = single(zeros(M,N,nframe));     
    v_ = single(zeros(M,N,nframe)); 
        
    L = single(zeros(M,N,RANK));
    pLx = single(zeros(M,N,RANK));
    pLy = single(zeros(M,N,RANK));
    
    if USE_DUAL_CC == 1
        qI = single(zeros(M,N,nframe,ch));
    else
        qI = [];
    end
        
    
  else
    rescale_factor_u = width_Pyramid{level+1}/width_Pyramid{level};
    rescale_factor_v = height_Pyramid{level+1}/height_Pyramid{level};
        
   parfor f = 1:nframe
        % prolongate to finer grid
        temp_u_(:,:,f) = imresize(u_(:,:,f),[M N], 'bilinear')/rescale_factor_u;  
        temp_v_(:,:,f) = imresize(v_(:,:,f),[M N], 'bilinear')/rescale_factor_v; 
    end
    u_ = (temp_u_); clear temp_u_;
    v_ = (temp_v_); clear temp_v_;
        
    for r= 1:RANK
        
        switch labls_Lresc(r)
            case 1 % current L(r) affects only u flow
                rescale_factor_L = rescale_factor_u;
            case 2 % current L(r) affects only v flow
                rescale_factor_L = rescale_factor_v;
            case 3 % current L(r) affects both u and v flows
                rescale_factor_L = (rescale_factor_u + rescale_factor_v)/2;
        end        
        
        L_temp(:,:,r) = imresize(L(:,:,r),[M N], 'bilinear')/rescale_factor_L;
        pLx_temp(:,:,r) = imresize(pLx(:,:,r),[M N], 'bilinear');
        pLy_temp(:,:,r) = imresize(pLy(:,:,r),[M N], 'bilinear');
    end
    L = (L_temp); clear L_temp; 
    pLx = (pLx_temp); clear pLx_temp; 
    pLy = (pLy_temp); clear pLy_temp;
    
    
    if USE_DUAL_CC == 1
        qI_host_ = gather(qI);
        for f = 1:nframe
            for ch = 1:size(I,4)
                qI_temp(:,:,f,ch) = imresize(qI_host_(:,:,f,ch),[M N], 'bilinear');
            end
        end
        qI = qI_temp;clear qI_temp qI_host_;
    else
        qI = [];
    end
        
  end 

  
  if ~isnan(theta_lndm)
      
      % scale of current pyramid, wrt ref:
      Spyr_u = width_Pyramid{level}/width_Pyramid{1};
      Spyr_v = height_Pyramid{level}/height_Pyramid{1};
      % we want to apply scale Spyr using a linear function of the form
      % x' = Spyr*x + a. The constant a is determined by the fact that,
      % if Spyr'=1/Spyr is an odd integer, we want x'=1 (1st pixel in low
      % resolution) to be the mapping of x=(Spyr'+1)/2 (middle of Spyr' * Spyr' block of pixels)
      % Therefore a=-(Spyr-1)/2 [the same argument applies if Spyr' is an
      % even integer]
      a_u = -(Spyr_u-1)/2;
      a_v = -(Spyr_v-1)/2;
      
      % landmarks coordinates, expressed in pixels (resolution) of
      % current pyramid level:
      landmarks_ref_curlev = landmarks_ref; % mem allocation
      landmarks_ref_curlev(:,1) = Spyr_u*landmarks_ref(:,1)+a_u;
      landmarks_ref_curlev(:,2) = Spyr_v*landmarks_ref(:,2)+a_v;
      
      
      [~, ~, inds_act_lndm3D, inds_lndm3D] = extract_lndm_mask(landmarks_ref_curlev,M,N,nframe);            
                  
      % rescale u_lndm to correspond to current scale and keep landmarks
      % that are active in current scale:
      u_lndm_curlev = u_lndm(inds_act_lndm3D,:)*Spyr_u;
      v_lndm_curlev = v_lndm(inds_act_lndm3D,:)*Spyr_v;
  else
      inds_lndm3D = [];
      u_lndm_curlev = [];
      v_lndm_curlev = [];
  end
  
  
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
  
  string_pyrlev = sprintf('%d/%d',end_pyr_level-level+1,pyramid_levels);
   
  % Warping iterations:
  iterate_warp((Qu),(Qv), ...
                                   (I_Pyramid_i), (I_Pyramid_ref), single(lambda), single(theta), single(epsilon), ...
                                    maxits, string_pyrlev, warps, ...
                                   g_Pyramid_i, single(nframe),single(RANK), ...
                                    kmedf, kwarp, kt, k1,k2,...
                                    nforce0,qI,USE_DUAL_CC, inds_lndm3D, u_lndm_curlev, v_lndm_curlev, theta_lndm);


end

if beg_pyr_level~=1
    % In this case, the specified MaxPIXpyr is smaller than the original
    % image resolution, therefore the final result will be computed as a
    % simple bilinear interpolation of the result in the resolution of
    % MaxPIXpyr pixels.
    
    
    % Interolate the coefficients (we don't care about the rest u,v since they will be cleared):           
    rescale_factor_u = width_Pyramid{beg_pyr_level}/width_Pyramid{1};    
    rescale_factor_v = height_Pyramid{beg_pyr_level}/height_Pyramid{1};    
    for r=1:RANK
        
        switch labls_Lresc(r)
            case 1 % current L(r) affects only u flow
                rescale_factor_L = rescale_factor_u;
            case 2 % current L(r) affects only v flow
                rescale_factor_L = rescale_factor_v;
            case 3 % current L(r) affects both u and v flows
                rescale_factor_L = (rescale_factor_u + rescale_factor_v)/2;
        end         
        
        L_temp(:,:,r) = imresize(L(:,:,r),[height_Pyramid{1} width_Pyramid{1}], 'bilinear')/rescale_factor_L;
    end
    L = (L_temp); clear L_temp;       
end
