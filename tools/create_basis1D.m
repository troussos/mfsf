function [basis basis_orth r] = create_basis1D(type_basis,r,F,nref)
% Create a 1D basis.
%
% type_basis: string determining the type of basis ('DCT', 'CubSplines','DSTc0')
% r: rank of the basis (number of columns)
% F: number of frames in the specific sequence
% nref (used only if type_basis='DSTc0'): frame considered as reference
%
% basis_orth orthonomalized basis (useful in the case that the
% original basis is not orthonormal - i.e. in B-Splines. Otherwise this
% output is exaclty as the first one)

switch  type_basis
        
    case 'DCT'                       
        basis = idct(eye(F));
        basis = basis(:,1:r);
                        
        if nargout > 1
            %  the basis is already orthonormal
            basis_orth = basis;
        end        
        
    case 'DSTc0'
        N1 = F-nref;
        N2 = nref-1;
                
        [bas1 freqs1] = matrix_dst(N1);
        bas1 = [zeros(N2,N1) ; bas1]; 
        
        [bas2 freqs2] = matrix_dst(N2);
        bas2 = [flipud(bas2) ; zeros(N1,N2) ];

        
        if N2<N1
           % swap the order so that 1 corrspnds to smallest-length DST:
           Ntemp = N1;
           bastemp = bas1;
           freqstemp = freqs1;
           
           bas1=bas2;
           N1=N2;
           freqs1 = freqs2;
           
           bas2 = bastemp;
           N2 = Ntemp;
           freqs2 = freqstemp;
        end
        
        maxdiff_combfreqs = 0;
                        
        j_freq_st = 1;
        basis = zeros(F-1,0);
        freqs_comb1 = zeros(1,0);
        freqs_comb2 = freqs_comb1;
        % logical array indicating if the correspondin index is the
        % beginning of a pair of combined basis elements:
        is_begofpair = false(1,0);
        for i_freq=1:N1
            %ind_fr2_comb: indice of freqs2 for the combination of current
            %freq from freqs1:
           [curdiff ind_fr2_comb] = min(abs(freqs1(i_freq) - freqs2));
           
           if curdiff>maxdiff_combfreqs
               maxdiff_combfreqs = curdiff;
           end
                        
           % elements of bas1 and bas2 to be combined:
           bas1_elcomb = bas1(:,i_freq);
           bas2_elcomb = bas2(:,ind_fr2_comb);
           
           basis = [basis , bas2(:,j_freq_st : (ind_fr2_comb-1)) ,...
               (bas1_elcomb + bas2_elcomb)/sqrt(2) , (bas1_elcomb - bas2_elcomb)/sqrt(2)];
            
           freqs_comb1 = [freqs_comb1 , nan(1,ind_fr2_comb-j_freq_st) , freqs1(i_freq) , freqs1(i_freq)];
           freqs_comb2 = [freqs_comb2 , freqs2(1,j_freq_st : (ind_fr2_comb-1)) , freqs2(ind_fr2_comb) , freqs2(ind_fr2_comb)];
           
           is_begofpair = [is_begofpair , false(1,ind_fr2_comb-j_freq_st) , true , false];
           
           j_freq_st = ind_fr2_comb+1;
        end
        
        basis = [basis , bas2(:,j_freq_st : end)];
        freqs_comb1 = [freqs_comb1 , nan(1,N2-j_freq_st+1)];
        freqs_comb2 = [freqs_comb2 , freqs2(1,j_freq_st : end )];
        
        is_begofpair = [is_begofpair , false(1,N2-j_freq_st+1)];
        
        fprintf('\nPairs of frequencies for the combined DST transform:\n');
        disp([freqs_comb1 ; freqs_comb2]);
                        
        fprintf('\nMaximum distance between combined frequencies:\n  comput. 1: %g\n  comput. 2: %g\n',...
            maxdiff_combfreqs,nanmax(abs(freqs_comb1-freqs_comb2)) );
        
        if r>=F
            warning(['Changing 1D rank from ' int2str(r) ' to ' int2str(F-1) ' since the latter corresponds to full rank']);
            r=F-1;            
        end    
        
        if is_begofpair(r)
            warning(['Changing 1D rank from ' int2str(r) ' to ' int2str(r+1) ' in order to have all ''frequency pairs''']);
            r=r+1;
        end
        
        basis = basis(:,1:r);
        
        if nargout > 1
            %  the basis is already orthonormal
            basis_orth = basis;
        end        
        
        
    case 'CubSplines'
        % Based on [Florent Brunet PhD thesis 2010]
                   
        if r<=3
           error('You need to define a rank greater than 3 for Cubic Splines.'); 
        end
        
        n = r-3;
        s = (F-1)/n;
        
        t=(0:F-1)';
        
        basis = zeros(F,r);
        for j=-3:n-1            
            basis(:,j+4) = basisfunc(t-j*s,s);
        end
        
        if nargout > 1
            %  orthogonalize the basis:
% %             [Q basis_orth] = qr(basis.');
% %             basis_orth = basis_orth.';                        
% %             
% %             for i=1:r
% %                 basis_orth(:,i) = basis_orth(:,i)/norm(basis_orth(:,i));
% %             end
            
            basis_orth = orth(basis);
        end
% %     case 'RAND'
% %         basis = rand(F,R);
end


function N = basisfunc(x,s)
% evaluate the basis function N_0(x) at the points given by the vector x
% s is the distance between knot points
%

% normalized x:
xn = x/s;

N = zeros(size(x));


mask = 0<=xn & xn<1;
% current active normalized and zero-based abscissa:
curxn = xn(mask);
N(mask) = curxn.^3/6;

mask = 1<=xn & xn<2;
% current active normalized and zero-based abscissa:
curxn = xn(mask)-1;
N(mask) = (-3*curxn.^3 + 3*curxn.^2 + 3*curxn + 1)/6;

mask = 2<=xn & xn<3;
% current active normalized and zero-based abscissa:
curxn = xn(mask)-2;
N(mask) = (3*curxn.^3 - 6*curxn.^2 + 4)/6;


mask = 3<=xn & xn<4;
% current active normalized and zero-based abscissa:
curxn = xn(mask)-3;
N(mask) = (-curxn.^3 + 3*curxn.^2 - 3*curxn + 1)/6;

function [matr_dst freqs1] =  matrix_dst(N1)
% compute the orthonormal matrix of DST transform:

% matr_dst =  idst(eye(N1));

freqs1 = (1:N1)/(2*(N1+1));
n=1:N1;

matr_dst = zeros(N1);
for i_frq=1:N1    
    curf = freqs1(i_frq);
    matr_dst(:,i_frq) = sin(2*pi*curf*n);
end


matr_dst = matr_dst ./ repmat( sqrt(sum(matr_dst.^2,1)) , N1,1 );
