function I = structure_texture_decomposition(I, texture, paramsSTD)
% Structure texture decomposition of image I.
%
% texture: logical indicating if you want to do decomposition. IF
% texture=0, this function does nothing and returns the input I
%
% paramsSTD (used only if texture=1): structure with params of the
% decomposition. It has the fields:
%
%   iters: number of iterations of ST Decomp, specifying also the type of decomp:
%   - if 0, a gaussian filter is applied
%   - if > 0, TV denoising is applied
%
%   scale: scale of the ST decomp: this defines the std dev of the
%   gaussian filter to be applied
%
%   factor: multiplying coefficient for STD, to derive
%   the final textural part of the image
%

if texture == 1
        
    sigma = paramsSTD.scale;
            
    % find the structural part u of the image I    
    u = imfilter(I, fspecial('gaussian', [15 15], sigma), 'replicate');

    I = I - paramsSTD.factor*u;
end