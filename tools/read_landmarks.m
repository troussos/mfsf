function shape = read_landmarks(filename_lndm)
% Read landmarks from the file filename_lndm

% read_shape pts
fid = fopen(filename_lndm);
tline = fgetl(fid);
start = 1;
while ~isstrprop(tline(1), 'digit')
    start = start + 1;
    tline = fgetl(fid);
end
end_line = start-1;
while isstrprop(tline(1), 'digit')
    end_line = end_line + 1;
    tline = fgetl(fid);
end

fclose(fid);

% read shape
shape =  dlmread(filename_lndm, ' ', [start-1 0 end_line-1 1]);

