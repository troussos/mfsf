function [basis, basis_orth] = create_basis(type_basis,R,F,dim ,nref )
% Create a basis for 2D or 3D signals (wrt the mapping domain).
%
%
% % % INPUTS:
%
% type_basis: string determining the type of basis ('EYE', 'DCT', 'CubSplines', 'DSTc0')
%
% R: rank of the basis (number of columns)
%
% F: number of frames in the specific sequence
%
% dim: 2 or 3, dimension that determines if the signals are 2D or 3D [default: dim=2]
%
% nref (used only if type_basis='DSTc0'): frame index (from 1 to F) considered as reference
%
% % % OUTPUTS:
%
% basis: 2F x R ((2F-2)xR in case of DSTc0) array whose columns contain the
% basis elements. In case DSTc0 the values at nref have been neglected
% (they are 0 anyway), that's way we have 2F-2 rows
%
% basis_orth: orthonormalized basis (useful in the case that the
% original basis is not orthonormal - i.e. in B-Splines. Otherwise this
% output is exactly as the first one)

if nargin<4
    dim=2;
end
if nargin<5
    nref=1;
end

switch  type_basis
    case {'EYE'}
        basis = eye(2*F);
        basis_orth = basis;
 
    case {'DCT','CubSplines','DSTc0'}
        
        if dim==2
            
            r = round(R/2);
            
            [basis basis_orth r] = create_basis1D(type_basis,r,F, nref);
            
            if strcmp(type_basis,'DSTc0');
                F=F-1;
            end
            
            basis = [basis zeros(F,r); zeros(F,r) basis];
            basis_orth = [basis_orth zeros(F,r); zeros(F,r) basis_orth];
            
        elseif dim==3
            
            r = round(R/3);
            
            [basis basis_orth r] = create_basis1D(type_basis,r,F, nref);
            
            if strcmp(type_basis,'DSTc0');
                F=F-1;
            end            
            
            basis = [basis zeros(F,r) zeros(F,r); zeros(F,r) basis zeros(F,r); zeros(F,r) zeros(F,r) basis ];
            
            basis_orth = [basis_orth zeros(F,r) zeros(F,r); zeros(F,r) basis_orth zeros(F,r); zeros(F,r) zeros(F,r) basis_orth ];                        
        end
end


