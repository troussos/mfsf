function labls_Lresc = find_labls_Lresc(Qu,Qv)
% find the labels that will control the rescaling of L coefficients (when
% we go from scale to scale)

RANK = size(Qu,2);

labls_Lresc = zeros(1,RANK);
for r=1:RANK
    
   if all(Qv(:,r)==0)
       labls_Lresc(r) = 1; % current L(r) affects only u flow
   elseif all(Qu(:,r)==0)
       labls_Lresc(r) = 2; % current L(r) affects only v flow
   else
       labls_Lresc(r) = 3; % current L(r) affects both u and v flows
   end
   
end