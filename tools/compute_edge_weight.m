function [g] = compute_edge_weight(f,paramsTVW)
% Computation of the space-varying weight g(x), which depends on the edges
% of the image f(x).
%
% Note: this function is a modification of a function from Thomas Pock's
% Matlab code "Duality-based TV-L1 Optical Flow".


[M, N, Nchan] = size(f);

q = 0.5;
alpha = paramsTVW.alpha;
sigma = paramsTVW.sigma;

if sigma
    ks = min(5, 2*(round(3*sigma))+1);
    f_g = imfilter(f,fspecial('gaussian',[ks ks], sigma),'replicate');
else
    f_g = f;
end

mask = [-1 0 1];

norm2 = f_g(:,:,1);
norm2(:) = 0;

for i_chan=1:Nchan
    fx = imfilter(f_g(:,:,i_chan),mask,'replicate');
    fy = imfilter(f_g(:,:,i_chan),mask','replicate');
    
    norm2 = norm2 + fx.^2 + fy.^2;
end
    
g = max(1e-06, exp(-alpha*norm2.^q));