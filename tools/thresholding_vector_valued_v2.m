function [u_,v_ ,qI] = thresholding_vector_valued_v2(Ix,Iy,It,u,v,u0,v0,lambda,theta,qI, ...
    inds_lndm3D, u_lndm, v_lndm, theta_lndm)
% Primal-dual algorithm that replaces of the thresholding scheme to generalise to       
% vector-valued image sequences (and/or in the case where landmarks are
% used). It minimises the corresponding energy (image data term + coupling
% term), which decouples for every spatiotemporal point.
%
% 
% theta_lndm: If NaN then no landmarks will be used. Otherwise this is the inverse weight of
% the landmarks term, for points with available landmarks. This will be
% used as quadratic prior in the energy that would drag the solution
% towards the landmarks: (1/(2*theta_lndm))*||u-u_lndm||^2. Its range is
% directly comparable to the parameter thet
%
% inds_lndm3D (used only if theta_lndm~=NaN): In case that input landmarks are available, this is a
% Nlandmarks*nframe-vector with the linear indices of the pixels in the
% reference frame that contain landmarks. These linear indices are to be
% applied be applied on the 3D arrays (Nrows x Ncols x nframe) u and v. For
% every frame they actually pick the same pixels. 
%
% u_lndm, v_lndm (used only if theta_lndm~=NaN): Nlandmarks*nframe-vectors with the u and v optical flow
% for points with landmarks, induced by the motion of the input landmark
% points (if any). If no landmarks available these arrays are [].
%


use_landmarks = ~isnan(theta_lndm);
if use_landmarks
    thetaTOtheta_lndm = theta/theta_lndm;
end
[M,N,nframe,nchannel] = size(It);

tau = 1;

qI = gpuArray(single(qI));
Ix = gpuArray(single(Ix));
Iy = gpuArray(single(Iy));
It = gpuArray(single(It));


reprojection = gpuArray(single(zeros(M,N,nframe)));    
for iter = 1:10
    
    u_ = gpuArray(u); v_ = gpuArray(v);
    for ch = 1:nchannel
        u_ = u_ - theta * lambda * (Ix(:,:,:,ch)) .* qI(:,:,:,ch);
        v_ = v_ - theta * lambda * (Iy(:,:,:,ch)) .* qI(:,:,:,ch);
    end

    if use_landmarks
        u_(inds_lndm3D) = ( thetaTOtheta_lndm*u_lndm + u_(inds_lndm3D))/ ( 1 + thetaTOtheta_lndm);
        v_(inds_lndm3D) = ( thetaTOtheta_lndm*v_lndm + v_(inds_lndm3D))/ ( 1 + thetaTOtheta_lndm);
    end
    
    
    tau = tau / 2;
                
    for ch = 1:nchannel
        qI(:,:,:,ch) = qI(:,:,:,ch) + lambda * tau *  (Ix(:,:,:,ch) .* ( u_-gpuArray(u0) ) + Iy(:,:,:,ch) .* ( v_-gpuArray(v0) ) + It(:,:,:,ch) );%, 0.00001);
        
        if ch==1
            reprojection = qI(:,:,:,ch) .* qI(:,:,:,ch);
        else
            reprojection = reprojection + qI(:,:,:,ch) .* qI(:,:,:,ch);
        end
    end
    reprojection = max(sqrt(reprojection),1);
    for ch = 1:nchannel
        qI(:,:,:,ch) = qI(:,:,:,ch) ./ reprojection;
    end
end
qI = gather(qI);
clear u0 v0 Ix Iy It reprojection;
end

