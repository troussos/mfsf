function solveMFSF_curImLin(Qu,Qv,...,
     I_x_el,  I_y_el,  I_t, ... 
     g, ...
     lambda, theta, epsilon, maxits,nframes,RANK, ...
     kt, k1,k2,...
     nforce0,qI,USE_DUAL_CC, inds_lndm3D, u_lndm, v_lndm, theta_lndm)
% Solve MFSF for current image linearisation. 
% Note that this code uses a simpler (and less mathematically sound)
% optimisation scheme than the one described in the corresponding
% publication (Garg, Roussos, Agapito, IJCV 2013). However, this scheme
% works well in practice.


global u_ v_ u0 v0 L pLx pLy ;
 
% stepwidth:
tau = 1.0/(4.0*theta+epsilon);

width = size(L,1);
height = size(L,2);


for k = 1:maxits
       
    u_cuda_GPU = (gpuArray(Qu) * (reshape(gpuArray(L(:)),width*height,RANK))')';
    u_cuda_GPU = reshape(u_cuda_GPU(:),width,height,nframes);
    
    v_cuda_GPU = (gpuArray(Qv) * (reshape(gpuArray(L(:)),width*height,RANK))')';
    v_cuda_GPU = reshape(v_cuda_GPU(:),width,height,nframes);
                
    
    % % % % Minimisation of the Energy wrt flow u - fitting to the image
    % % % % data term (decoupling for every spatiotemporal point ): 
            
    if( USE_DUAL_CC ==0 )
        % thresholding scheme:
        [u_cuda_GPU,v_cuda_GPU] = feval( kt, I_x_el, I_y_el, I_t, u_cuda_GPU, v_cuda_GPU, u0, v0, lambda, theta, width, height, nframes);
    else
        % replacement of the thresholding scheme to generalise to
        % vector-valued image sequences (and/or in the case where landmarks are used):
        [u_cuda_GPU,v_cuda_GPU , qI] = thresholding_vector_valued_v2((I_x_el), (I_y_el), (I_t), u_cuda_GPU, v_cuda_GPU, (u0), (v0), max(lambda), theta, (qI),...
            inds_lndm3D, u_lndm, v_lndm, theta_lndm);
    end
        
    
    if ~isempty(nforce0)
        % forcing this estimation to have 0 flow at reference img:
        u_cuda_GPU(:,:,nforce0) = 0;v_cuda_GPU(:,:,nforce0) = 0;
    end
    
    
        
    % % % % Minimisation of the Energy wrt the trajectory coefficients
    % % % % L(x), i.e. spatial regularisation (decouplinf for every coefficient):
    
    u_ = gather(u_cuda_GPU);
    v_ = gather(v_cuda_GPU);
           
    
    % Projection of the current flow u on the trajectory subspace:
    L_cuda_GPU = ([Qu;Qv]' * [ (reshape(u_cuda_GPU(:),width*height,nframes))' ; (reshape(v_cuda_GPU(:),width*height,nframes))'])';
    
    L_cuda_GPU = reshape(L_cuda_GPU(:),width,height,RANK);

    
    % % % Spatial regularisation of the projected flow:
    
    % primal step of the primal-dual scheme:
    L_cuda_GPU = feval(k1, L_cuda_GPU, pLy, pLx, nframes, RANK, width, height, RANK, theta);
    L = gather(L_cuda_GPU);
    clear 'L_cuda_GPU';            
    
    % dual step of the primal-dual scheme:
    [pLy_cuda_GPU, pLx_cuda_GPU]  = feval(k2, L, pLy, pLx, g, nframes, RANK, width, height, RANK, theta, tau, epsilon);
    pLx = gather(pLx_cuda_GPU);
    pLy = gather(pLy_cuda_GPU);
    clear 'pLx_cuda_GPU' 'pLy_cuda_GPU';
    

end
